function[alta baja] = Matriz_hankel(list)
hankel = [list'(1:(end-1));list'(2:(end))];

[u,s,v] = svd(hankel);
H = s(1)*u(:,1)*v(:,1)';
H2 = s(4)*u(:,2)*v(:,2)';

baja = [H(1,1) (H(1,2 : end)+H(2,1:end-1))/2 H(2,end)];
alta = [H2(1,1) (H2(1,2 : end)+H2(2,1:end-1))/2 H2(2, end)];


endfunction