function [mse rmse mNSE R_2] = calculoError(x,x2)
  R2=0;
  e = x-x2;
  mse = mean(e.^2);
  rmse = sqrt(mse);
  mNSE = 0;
  sum_e=sum(abs(e));
  sum_x_mean=0;
  for i =[1:length(x)]
  i;
  sum_x_mean += abs(x(i) -mean(x));
  R2 += (((x(i)-mean(x)) * (x2(i)-mean(x2)))/(sqrt(var(x2))*sqrt(var(x))));
  endfor
  R2/=(length(x)-1);
  R_2=R2^2;
  mNSE = 1- (sum_e/sum_x_mean);
  endfunction
