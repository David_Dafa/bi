function [training_set test_set training_set2 test_set2 m] = cargarDatos(data,H)
  [alta baja] = Matriz_hankel(data);
  temp=baja';
  temp2=alta';
  
  matrix = [temp];
  matrix2 = [temp2];
  #iterate creating model
  for m = 1:30

   train_lenght = floor(rows(matrix)* 0.8);
   training_set{m} = regrex(m,H,baja(1:train_lenght));
   #20%
   test_set{m} = regrex(m,H,baja((train_lenght+1):end));
   training_set(m);
   test_set(m);
   
   %alta 80%
   train_lenght = floor(rows(matrix2)* 0.8);
   training_set2{m} = regrex(m,H,alta(1:train_lenght));
   #20%
   test_set2{m} = regrex(m,H,alta((train_lenght+1):end));
   training_set2(m);
   test_set2(m);
   m;
   
  endfor
endfunction

