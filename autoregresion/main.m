function main()
  data = load("dataSet.txt");
  H= input("H: ");
  #data = [1:10]';
  [training_set testing_set training_set2 testing_set2 m]= cargarDatos(data, H);
  plot_data.r_2 = [];
  plot_data.rmse = [];
  plot_data.mNSE = [];
  plot_data.gcv = [];
  #[1:length(training_set)];
  for i= [1:(length(training_set))]
    i;
    #baja
    training_matrix = cell2mat(training_set(i));
    Y = training_matrix(:,1);
    test_matrix = cell2mat(testing_set(i));
    training_matrix = training_matrix(:,2:end);
    A2 = Y' * pinv(training_matrix)';
    Y_est_low = test_matrix(:,2:end) * A2';
    Y_testing= test_matrix(:,1);
    
    #alta/arx
    training_matrix2 = cell2mat(training_set2(i));
    Y2 = training_matrix2(:,1);
    test_matrix2 = cell2mat(testing_set2(i));
    training_matrix2 = [training_matrix2(:,2:end) training_matrix];
    A22 = Y2' * pinv(training_matrix2)';
    Y_est2 = [test_matrix2(:,2:end) test_matrix(:,2:end)]* A22';
    Y_testing2= test_matrix2(:,1);
    
    
    y_testing_total = Y_testing + Y_testing2;
    y_est_total = Y_est_low + Y_est2;
      
    
    [mse rmse mNSE R_2]=calculoError(y_testing_total,y_est_total);
    plot_data.r_2(i) = R_2;
    plot_data.rmse(i) = rmse;
    plot_data.mNSE(i) = mNSE;
    plot_data.gcv(i) = (mse)/(1-(i/length(y_testing_total)));
    plot_data.y_est_total{i}= [y_est_total];
    plot_data.y_testing_total{i}= y_testing_total;
    

endfor
#Grafica el horizonte y el error
plot_data.r_2(:)
plot_data.gcv(:)
figure(2)
plot([1:m],plot_data.r_2(:),"-r2-")
xlabel('lag')
ylabel('-R2-')
title('R2')


figure(4)
plot([1:m],plot_data.mNSE(:))
xlabel("lag")
ylabel("Valor")
title("mNSE")

figure(5)
plot([1:m],plot_data.gcv(:),'-xb')
xlabel("lag")
ylabel("Valor")
title("gcv")

model= input("Model: ")
plot_data.rmse(model)
plot_data.mNSE(model)
plot_data.r_2(model)
figure(1);
Y_est= cell2mat(plot_data.y_est_total(model));
Y_testing = cell2mat(plot_data.y_testing_total(model));
plot(Y_est(1:end),'-r')
hold on;
plot(Y_testing(1:end),'-b')
legend('Estimado', 'Real')
grid on
xlabel('Valor')
ylabel('Horizonte')
hold off;
title('Estimacion')

p = polyfit(y_est_total,y_testing_total((length(y_testing_total) - length(y_est_total))+1:end),1);
f = polyval(p,y_est_total);
figure(6)
hold on;
plot(y_est_total,y_testing_total((length(y_testing_total))-length(y_est_total)+1:end),'o');
plot(y_est_total,f,'-r');
plot(y_testing_total((length(y_testing_total))-length(y_est_total)+1:end),y_testing_total((length(y_testing_total))-length(y_est_total)+1:end),':b');
hold off;
xlabel('X');
ylabel('Y');
title('AR');
legend('data point', 'best lienar fit', 'Y=X');
set(gca,'XTick',[0:5:100]);
set(gca,'YTick',[0:5:100]);
grid on

endfunction
