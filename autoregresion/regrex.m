function vr = regrex(mem, hor, x)
  m = mem+hor-1;  
  N = length(x);
  vr = zeros(N-m,m+1);
  for i=1+m:length(x)
    for j = 1:m+1
        vr(i,j) = x(i-j+1);
    endfor
  endfor 
  vr = vr(m+1:end,:);
endfunction