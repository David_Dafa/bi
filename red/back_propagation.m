function [mlp] = back_propagation(y_train, x_train, hNodos,mlp)
  a = sqrt(6/(hNodos+mlp.memoria+mlp.h-1));
  w1 = (rand(mlp.memoria+mlp.h-1,hNodos)*2*a)-a;
  H = 1 ./ (1 + e.^-(w1'*x_train));  
  w2 = pseudoinversa(H, y_train, mlp.C,mlp.mu);
  for i = 1:mlp.epocas
    z=w2*H;  %y_est
    E=y_train-z; %error
    dOut=E;
    eh=w2'*dOut;
    dw1=(H-H.^2).*eh;
    dw1=dw1*x_train';
    w1=w1-(mlp.mu*dw1)';%- algo (mu)*dw1%;
    H = 1 ./ (1 + e.^-(w1'*x_train));
    w2 = pseudoinversa(H, y_train,mlp.C,mlp.mu);
  endfor
  mlp.w1 = w1;
  mlp.w2 = w2;
endfunction
