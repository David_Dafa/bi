clc;
clear;
Vtrain = [];
Vtest = [];
mlp = struct(
  "epocas", 50,
  "nodes_init" , 1,
  "nodes_end" , 10,
  "w1", 0,
  "w2", 0,
  "memoria",9,
  "C", 10^(8),
  "Run", 5,
  "nivelHankel", 8,
  "mu", 0.001 ,
  "h", 4
);

train = 80;
test = 20;

data = dlmread('Data4.dat');

datosTrain = int64(length(data)*(train/100));
datosTest = int64(length(data)*(test/100));


for i = 1:datosTrain
  Vtrain(i) = data(i);
endfor

for i = 1:datosTest
  Vtest(i) = data(datosTrain+i);
endfor

%Para Training%

[low_frec high_frec] = svd(Vtrain',mlp);

dataLow = regrex(mlp.memoria,mlp.h,low_frec);
y_trainLow = dataLow(:,1)';%Rellenar%
x_trainLow = dataLow(:,2:end)';%Rellenar%

[weights_l{mlp.nodes_end} y_est_low ErrorPromTopologia(mlp.nodes_end)] = mlpTrain(y_trainLow,x_trainLow,mlp);  
ErrorFit(mlp.nodes_end-1) = ErrorPromTopologia(mlp.nodes_end-1)/ErrorPromTopologia(mlp.nodes_end);

save w_l weights_l
figure(1);
plot([1:mlp.nodes_end-1],ErrorFit,'-b');
xlabel('nodos')
ylabel('error')
title('baja freq')
  
 
datahigh = regrex(mlp.memoria,mlp.h,high_frec);
y_trainHigh = dataLow(:,1)';%Rellenar%
x_trainHigh = dataLow(:,2:end)';%Rellenar%
printf("\n-----------------High------------------\n")

[weights_h{mlp.nodes_end} y_est_high ErrorPromTopologia_h(mlp.nodes_end)] = mlpTrain(y_trainHigh,x_trainHigh,mlp);


ErrorFit_h(mlp.nodes_end-1) = ErrorPromTopologia_h(mlp.nodes_end-1)/ErrorPromTopologia_h(mlp.nodes_end);


save w_h weights_h
figure(2);
plot([1:mlp.nodes_end-1],ErrorFit_h,'-b');
xlabel('nodos')
ylabel('error')
title('Alta freq')

