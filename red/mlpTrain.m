function [weights ff ErrorProm] = mlpTrain(y_trainig, x_training, mlp)
  cont= 1;

    for node = mlp.nodes_init:mlp.nodes_end
      fprintf('\n Training MLP with Hidden Node : %d ', node)
      aux = 1e2;
      for j = 1:mlp.Run
        mlp = back_propagation(y_trainig, x_training,node, mlp);
        
        %Testing%
        ff = mlp_ff(x_training, mlp);
        fit(j) = sqrt(mse(y_trainig-ff));
        auxError(node) = fit(j);
        if (fit <	aux)
          bestMlp = mlp;
          aux = fit(j);
          if aux < 1e-5
            break;
          endif
        endif
    endfor
    AuxErrorProm(node) = mean(auxError);
    weights(node) = bestMlp;
  endfor
  ErrorProm = mean(AuxErrorProm);
endfunction
