function [y] = mlp_ff(x_train, mlp)
  fv = mlp.w1'*x_train;
  h = 1 ./ (1 + e.^-(fv));
  y = mlp.w2*h;
endfunction
