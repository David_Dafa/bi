function [mse] = mse(x)
  mse = mean(x.^2);
endfunction
