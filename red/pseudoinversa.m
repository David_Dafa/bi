function [B] = pseudoinversa(H, y_train, C,mu)
  if C == 0
    B = (H*H')/(H*y_train);
    B = B';
  else
    [L N] = size(H);
    if L > N
      % w = pinv(H * H' + I/C) * H' * Y ; L > N
      hh = bsxfun(@plus, (speye(L)./C),H*H');
      hy = H*y_train;
      B = hh\hy;
      B =  B';
    else
      % w = H' * pinv(H' * H + I/C) * Y   ; L < N   
      hh = bsxfun(@plus,(mu*(speye(N))),H'*H);
      hy = hh\y_train';
      B = H*hy;
      B =  B';
    endif
  endif
endfunction
