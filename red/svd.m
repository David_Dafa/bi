function [high_freq low_freq] = svd(data, mlp)
  l = mlp.nivelHankel;
  %HSVD
  cols = length(data) - 1;
  j = 2;
  serie = data;
  for i = 1:l
    H = [data'(1:(end-1));data'(2:(end))];

    [U S V] = svd(H);
    sdiag = diag(S);
    v = V(:,1:length(sdiag));

    H1 = sdiag(1)*U(:,1)*V(:,1)';
    C(1,:) = [H1(1,1) (H1(1,2:end)+H1(2,1:end-1))/2 H1(2,end)];

    H2 = sdiag(2)*U(:,2)*V(:,2)';
    C(j,:) = [H2(1,1) (H2(1,2:end)+H2(2,1:end-1))/2 H2(2,end)];

    serie = C(1,:);
    j = j+1;
  endfor
  
  %Low Frequency 
  low_freq = C(1,:)';
  
  %High Frequency
  high_freq = C(2,:)';
  for i = 1:l-1
      high_freq = high_freq + C(i+2,:)';
  endfor
  
endfunction
